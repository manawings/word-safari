﻿using UnityEngine;
using System.Collections;

public class Compliment : MonoBehaviour {
    public Animator anim;
    public SpriteRenderer sRenderer;
    public enum Type { Amazing, Excellent, Fantastic, GoodJob, WellDone };
    public Sprite[] sprites;

    public void Show(Type type)
    {
        if (!IsAvailable2Show()) return;

        sRenderer.sprite = sprites[(int)type];
        anim.SetTrigger("show");
    }

    private bool IsAvailable2Show()
    {
        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);
        return info.IsName("Idle");
    }
}
