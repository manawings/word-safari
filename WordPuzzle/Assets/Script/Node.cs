﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Node : MonoBehaviour
{
    [SerializeField] private Image _nodeImage;
    [SerializeField] private Text _letter_text;

    public Vector3 OriginPosition;
    public Vector3 LocalPosition;

    public event Action OnEndDrag;
    public event Action<Node> OnDragAction;
    public event Action OnBeginDragUI;
    public event Action OnEndDragUI;


    public const float RADIUS = 50.0f;

    public char Letter { get; set; }
    public bool IsConnect { get; set; }
    private Node previousNode;

    public Node PreviousNode
    {
        get
        {
            return previousNode;
        }

        set
        {
            previousNode = value;
        }
    }


    public Node()
    {
        IsConnect = false;
        PreviousNode = null;
    }

    public void Calibrate(List<Node> nodes, Color color,char value)
    {
        OriginPosition = transform.position;
        LocalPosition = transform.localPosition;       
        SetValue(value);
    //    SetColor(color);
    }

    private void SetValue(char letter)
    {
        Letter = letter;
       // _letter_text.text = letter.ToString();
        _letter_text.text = "";

        _nodeImage.sprite = Resources.Load<Sprite>("Font/"+letter);
    }

    public void Reset()
    {
        IsConnect = false;
        PreviousNode = null;
    }

    private void SetColor(Color color)
    {
        _nodeImage.color = color;
    }

    public void OnDrag()
    {
        if (OnDragAction != null) OnDragAction(this);
        if (OnBeginDragUI != null) OnBeginDragUI();
    }

    public void EndDrag()
    {
        if (OnEndDrag != null) OnEndDrag();
        if (OnEndDragUI != null) OnEndDragUI();
    }
}
