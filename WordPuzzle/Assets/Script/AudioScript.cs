﻿using UnityEngine;

public class AudioScript : MonoBehaviour
{
    [SerializeField] private AudioClip _audioClip;
    [SerializeField] private AudioSource _audioSource;

    // Use this for initialization
    void Start ()
    {
        _audioSource.clip = _audioClip;
        _audioSource.Play();
    }

}
