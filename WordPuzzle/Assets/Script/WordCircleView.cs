﻿using System;
using System.Collections.Generic;
using SA.Common.Animation;
using UnityEngine;
using UnityEngine.UI;

public class WordCircleView : MonoBehaviour
{
    [SerializeField] public Text _label;
    [SerializeField] public Image _labelFrame;
    [SerializeField] private float _nodeRadius = 50f;
    [SerializeField] private Line _prefabLine;
    [SerializeField] private Node _prefabNode;
    private float _radius = 0.64f;
    [SerializeField] private float _localRadius = 250.0f;

    [SerializeField] private LineRender _lineRender;
    [SerializeField] private TextPreview _textPreview;
    private char[] _alphabets;

    private readonly Color[] _colorList = new Color[]
        {Color.red, Color.yellow, Color.green, Color.blue, Color.cyan, Color.white, Color.magenta};

    private int _currentLineIndex = 0;
    private Node _currentNode;
    public GameController gameController;
    private bool _isFirstDrag = true;

    private List<Line> _lines = new List<Line>();
    private int _maximumLine = 0;
    private List<Node> _nodes = new List<Node>();
    private string _selectedWord = null;
    private int _nodeConnectedCount = 0;
    public List<Vector3> letterPositions = new List<Vector3>();


    const float REVESE_ANGLE = 180f;
    const float REVESE_ANGLE_RANGE = 15f;

    public event Action OnNodeConnected;
    
    public bool IsFirstDrag
    {
        get { return _isFirstDrag; }

        set { _isFirstDrag = value; }
    }

    public void Calibrate(char[] alphabets)
    {
        _alphabets = alphabets;
        InitNode(alphabets.Length);
        _lineRender.Calibrate(_radius,letterPositions,this.transform.position);
        _lineRender.OnRelease += CheckAnswer;
        _textPreview.SetLetter(alphabets);

    }

    //private void InitLine(int maximumLine)
    //{
    //    for (int i = 0; i < maximumLine; i++)
    //    {
    //        var newLine = Instantiate<Line>(_prefabLine, transform);
    //        _lines.Add(newLine);
    //    }
    //    _currentLineIndex = 0;
    //}

    //private void SetSize()
    //{
    //    float diameter = _radius * 2;
    //    _circleImage.rectTransform.sizeDelta = new Vector2(diameter, diameter);
    //}

    private void InitNode(int nodeNumber)
    {
        for (int i = 0; i < nodeNumber; i++)
        {
            var angle = i * Mathf.PI * 2 / nodeNumber;
            var pos = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0) * (_localRadius - _nodeRadius);
            var newNode = Instantiate<Node>(_prefabNode, transform);
            newNode.transform.localPosition = pos;
            newNode.Calibrate(_nodes, _colorList[i % _colorList.Length], _alphabets[i % _alphabets.Length]);
            newNode.OnBeginDragUI += _lineRender.Dragging;
            newNode.OnEndDragUI += _lineRender.Release;
            _nodes.Add(newNode);
            letterPositions.Add(newNode.transform.position);

        }
    }


    //private void DrawLine(Node nodeFrom)
    //{
    //    Debug.Log("drag");

    //    if (_currentLineIndex >= _maximumLine || InputController.IsPause)
    //        return;

    //    if (IsFirstDrag)
    //    {
    //        _lines[_currentLineIndex].SetOriginPosition(nodeFrom.OriginPosition);
    //        _currentNode = nodeFrom;
    //        UpdateSelectedWord(_currentNode.Letter.ToString());

    //    }

    //    var differenceVector = InputPosition - _currentNode.OriginPosition;
    //    _lines[_currentLineIndex].DrawLine(differenceVector);
    //    CheckNodeToConnect(_currentNode);
    //    IsFirstDrag = false;
    //}

    //private Vector3 InputPosition
    //{
    //    get
    //    {
    //        var mousePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    //        mousePoint.z = 90;
    //        return mousePoint;
    //    }
    //}


    //private void DrawLine(Node nodeFrom, Node nodeTo)
    //{
    //    var differenceVector = nodeTo.OriginPosition - nodeFrom.OriginPosition;

    //   _lines[_currentLineIndex].DrawLine(differenceVector);

    //    if (_currentLineIndex < _maximumLine - 1)
    //    {
    //        ++_currentLineIndex;
    //        _lines[_currentLineIndex].SetOriginPosition(nodeTo.OriginPosition);
    //        _currentNode = nodeTo;
    //    }
    //}

    //public void CheckNodeToConnect(Node nodeFrom)
    //{
    //    //var input = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
    //    var input = InputPosition;

    //    if (nodeFrom.IsConnect)
    //        return;

    //    for (int i = 0; i < _nodes.Count; i++)
    //    {
    //        if (_nodes[i] != nodeFrom)
    //        {
    //            var distance = Vector2.Distance(input, _nodes[i].OriginPosition);
    //            if (distance < _nodeRadius)
    //            {
    //                if (CheckDestinationIsNotAlreadyConnect(i))
    //                {
    //                    ConnectToDestinationNode(nodeFrom, i);
    //                    return;
    //                }
    //            }
    //            if (CheckPointBackToPreviousNode(nodeFrom, i, distance))
    //            {
    //                RevertToPreviousNode(nodeFrom);
    //                return;
    //            }
    //        }
    //    }
    //}

    //private void RevertToPreviousNode(Node nodeFrom)
    //{
    //    _currentNode = nodeFrom.PreviousNode;
    //    _lines[_currentLineIndex].Reset();
    //    _currentLineIndex--;
    //    BackwardSelectedWord();
    //    nodeFrom.PreviousNode.IsConnect = false;
    //    nodeFrom.PreviousNode = null;
    //    _nodeConnectedCount--;
    //}

    //private bool CheckPointBackToPreviousNode(Node nodeFrom, int i, float distance)
    //{
    //    if (_nodes[i] == nodeFrom.PreviousNode && distance > Node.RADIUS)
    //    {
    //        float diffAngle = Mathf.Abs(_lines[_currentLineIndex - 1].angle - _lines[_currentLineIndex].angle);
    //        return Mathf.Abs(REVESE_ANGLE - diffAngle) < REVESE_ANGLE_RANGE;
    //    }
    //    return false;
    //}

    //private bool CheckDestinationIsNotAlreadyConnect(int i)
    //{
    //    return !_nodes[i].IsConnect;
    //}

    //private void ConnectToDestinationNode(Node nodeFrom, int i)
    //{
    //    if (OnNodeConnected != null) OnNodeConnected();
    //    var connectedNode = _nodes[i];
    //    DrawLine(nodeFrom, connectedNode);
    //    nodeFrom.IsConnect = true;
    //    connectedNode.PreviousNode = nodeFrom;
    //    UpdateSelectedWord(connectedNode.Letter.ToString());
    //    _nodeConnectedCount++;
    //}

    //private void EndDrawLine()
    //{
    //    if (InputController.IsPause) return;
    //    RemoveUnconnectedLine();
    //    CheckAnswer();
    //}

    //private void RemoveUnconnectedLine()
    //{
    //    if (_currentLineIndex >= _nodeConnectedCount)
    //    {
    //        _lines[_currentLineIndex].Reset();
    //    }
    //}

    private void CheckAnswer(string answer)
    {
        InputController.Lock();
        var lineColor = gameController.CheckAnswer(answer);
        _textPreview.SetColor(lineColor);
        if (lineColor == Color.red)
            ShakeNodes();
 //       foreach (var line in _lines)
 //           line.SetColor(lineColor);
  
    }

    void ShakeNodes()
    {
        foreach (var node in _nodes)
        {
            iTween.ShakePosition(node.gameObject,new Vector3(0.1f,0.1f,0.1f),0.5f);
        }


    }

    public void Reset()
    {
        InputController.Unlock();
        _textPreview.FadeOut();
    }

    //public void Reset()
    //{
    //    _nodeConnectedCount = 0;
    //    InputController.Unlock();
    //    IsFirstDrag = true;
    //    _currentLineIndex = 0;
    //    ClearSelectedWord();
    //    foreach (var line in _lines)
    //        line.Reset();
    //    foreach (var node in _nodes)
    //        node.Reset();
    //}

    //private void UpdateSelectedWord(string s)
    //{
    //    _selectedWord += s;
    //    SetLabel();
    //}

    //private void BackwardSelectedWord()
    //{
    //    _selectedWord = _selectedWord.Remove(_selectedWord.Length - 1);
    //    SetLabel();
    //}

    //private void ClearSelectedWord()
    //{
    //    _selectedWord = "";
    //    SetLabel();
    //}

    //private void SetLabel()
    //{
    //    _label.text = _selectedWord;
    //    AdjustLabelFrameSize(Color.black);
    //}

    //void AdjustLabelFrameSize (Color color)
    //{
    //    _labelFrame.color = color;
    //    _labelFrame.rectTransform.sizeDelta = new Vector2(_label.preferredWidth + 10f, _label.preferredWidth + 50f);

    //}

}