﻿using System.Collections.Generic;
using UnityEngine;

public class Profile : MonoBehaviour
{
    public const int HINT_COST = 25;
    public const int GOLD_PER_LEVEL = 50;
    public const int EXTRA_WORD_BONUS = 100;

    public static Profile _instance;
    private TextAsset _asset;
    private int _gold = 500;
    private IList<string[]> _subLevelList;
    private DataController _dataController;
    public bool hasLoadData = false;

    public Profile()
    {
    }

    public static Profile Instance
    {
        get
        {
            if (_instance == null)
            {
                var container = new GameObject("ProfileController");
                _instance = container.AddComponent<Profile>();
                _instance.Init();
                DontDestroyOnLoad(container);
            }
            return _instance;
        }
    }

    public LevelData CurrentLevelData { get; set; }

    public int CurrentSubLevel { get; set; }
    public int CurrentMainLevel { get; set; }

    public int MaxSubLevel { get; set; }
    public int MaxMainLevel { get; set; }

    public List<string> CurrentPlayedWords { get; set; }
    public List<string> HavePlayedExtrawords { get; set; }
    public string CurrentTextAssetName { get; set; }

    public int Gold
    {
        get { return _gold; }
        set { _gold = value; }
    }


    //public TextAsset Asset
    //{
    //    get { return _asset; }
    //    set
    //    {
    //       _asset = value;
    //       CurrentTextAssetName = _asset.name;
    //       SubLevelList = CSVParser.ReadAsList(_asset.text);
    //    }
    //}

    private void Init()
    {
        CurrentPlayedWords = new List<string>();
        HavePlayedExtrawords = new List<string>();
       _dataController = new DataController();
        LevelManager.instance.Load();

        if (hasLoadData)
        {
            LevelManager.instance.SetMaxProgress(MaxMainLevel, MaxSubLevel);
            CurrentLevelData = LevelManager.instance.GetMainLevel(CurrentMainLevel);
            CurrentLevelData.currentLevel = CurrentSubLevel;
            // CurrentLevelData = LevelManager.instance.leve
            //  Asset = Resources.Load<TextAsset>("CSV/" + CurrentTextAssetName);
        }
    }
            
    public static void Save()
    {
        _instance._dataController.Save();   
    }
}