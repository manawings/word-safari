﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    private static bool _isPause = false;

    public static bool IsPause
    {
        get
        {
            return _isPause;
        }

        set
        {
            _isPause = value;
        }
    }

    public static void Lock()
    {
        IsPause = true;
    }

    public static void Unlock()
    {
        IsPause = false;
    }
}
