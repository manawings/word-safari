﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageManager : MonoBehaviour {

    private const string LANGUAGE_KEY = "LANGUAGE_KEY";
    public static LanguageManager instance = null;
    private string _currentLanguage = "EN";
    public enum Language { TH,EN };


    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    public void SetLanguage(Language language)
    {
        PlayerPrefs.SetString(LANGUAGE_KEY, language.ToString());
    }

    public string CurrentLanguage
    {
        get
        {
            return (PlayerPrefs.GetString(LANGUAGE_KEY, "EN"));
        }
    }
}
