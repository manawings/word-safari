﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData  {

    public Sprite iconSprite;
    public Sprite iconSpriteActive;

    public Sprite nameSprite;
    private IList<string[]> _subLevelList;
    private TextAsset _asset;
    public int maxLevel;
    public int currentLevel;
    public int index;
    public string name;

    public void Calibrate(TextAsset asset, Sprite iconSprite, Sprite iconSpriteActive,Sprite nameSprite,int index,bool hasClear,int currentLevel = 0)
    {
        this.index = index;
        this.iconSprite = iconSprite;
        this.iconSpriteActive = iconSpriteActive;
        this.nameSprite = nameSprite;
        Asset = asset;
        if (hasClear)
            maxLevel = SubLevelList.Count - 1;
        else
            this.currentLevel = currentLevel;
    }

    public void Complete ()
    {
        maxLevel = SubLevelList.Count - 1;
    }

    public void Progress ()
    {
        currentLevel++;
        if (currentLevel > maxLevel)
            maxLevel++;
    }

    public float PercentProgress {
    get
        {
            return maxLevel / (SubLevelList.Count - 1);
        }
    }

    public bool HasClear {
        get
        {
            return maxLevel >= SubLevelList.Count - 1;
        }
    }

    public IList<string[]> SubLevelList
    {
        get { return _subLevelList ?? (_subLevelList = new List<string[]>()); }
        set { _subLevelList = value; }
    }

    public TextAsset Asset
    {
        get { return _asset; }
        set
        {
            _asset = value;
            this.name = _asset.name;
            SubLevelList = CSVParser.ReadAsList(_asset.text);
        }
    }
}
