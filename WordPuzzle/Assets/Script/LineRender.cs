﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRender : MonoBehaviour
{
    [SerializeField] private TextPreview _textPreview;
    private LineRenderer _displayLineRenderer;
    private float _radius;
    public event Action<string> OnRelease;


    public void Calibrate(float radius, List<Vector3> letterPositions,Vector3 midPoint)
    {
        _radius = radius;
        this.letterPositions = letterPositions;
        _displayLineRenderer = GetComponent<LineRenderer>();
    }

    public void Dragging()
    {
        if (InputController.IsPause) return;

        if (!isDragging)
        {
            _textPreview.SetText("");
            _textPreview.FadeIn();
            isDragging = true;
        }

        int nearest = GetNearestPosition(InputPosition, letterPositions);
        Vector3 letterPosition = letterPositions[nearest];
        if (Vector3.Distance(letterPosition, InputPosition) < _radius)
        {
            if (currentIndexes.Count >= 2 && currentIndexes[currentIndexes.Count - 2] == nearest)
            {
                currentIndexes.RemoveAt(currentIndexes.Count - 1);
                _textPreview.SetIndexes(currentIndexes);
            }
            else if (!currentIndexes.Contains(nearest))
            {
                currentIndexes.Add(nearest);
                SoundManager.instance.PlayButtonSound();
                _textPreview.SetIndexes(currentIndexes);
            }
        }

        BuildPoints();

        if (vertexs.Count >= 2 && isDragging)
        {
            curvePositions = iTween.GetSmoothPoints(vertexs.ToArray(), smoothFactor);
            DrawLineRenderer(curvePositions.ToArray());
        }
    }

    public void Release ()
    {
        isDragging = false;
        currentIndexes.Clear();
        _displayLineRenderer.positionCount = 0;
        if (OnRelease != null) OnRelease(_textPreview.GetText());
    }

    private bool isDragging;
    //void Update()
    //{
    //    if (InputController.IsPause) return;
    //    if (Input.GetMouseButtonDown(0))
    //    {
    //        _textPreview.SetText("");
    //        _textPreview.FadeIn();
    //    }
    //    if (Input.GetMouseButton(0))
    //    {
    //        isDragging = true;
    //     //   _labelFrame.gameObject.SetActive(true);

    //        int nearest = GetNearestPosition(InputPosition, letterPositions);
    //        Vector3 letterPosition = letterPositions[nearest];
    //        if (Vector3.Distance(letterPosition, InputPosition) < _radius)
    //        {
    //            if (currentIndexes.Count >= 2 && currentIndexes[currentIndexes.Count - 2] == nearest)
    //            {
    //                currentIndexes.RemoveAt(currentIndexes.Count - 1);
    //                _textPreview.SetIndexes(currentIndexes);
    //            }
    //            else if (!currentIndexes.Contains(nearest))
    //            {
    //                currentIndexes.Add(nearest);
    //                SoundManager.instance.PlayButtonSound();
    //                _textPreview.SetIndexes(currentIndexes);
    //            }
    //        }

    //        BuildPoints();
    //    }
    //    else if (Input.GetMouseButtonUp(0))
    //    {
    //        isDragging = false;
    //        currentIndexes.Clear();
    //        _displayLineRenderer.positionCount = 0;
    //        if (OnRelease != null) OnRelease(_textPreview.GetText());
    //    }

    //    if (vertexs.Count >= 2 && isDragging)
    //    {
    //        curvePositions = iTween.GetSmoothPoints(vertexs.ToArray(), smoothFactor);
    //        DrawLineRenderer(curvePositions.ToArray());
    //    }
    //}

    private List<Vector3> vertexs = new List<Vector3>();
    public List<int> currentIndexes = new List<int>();
    public List<Vector3> letterPositions = new List<Vector3>();
    public List<Vector3> curvePositions = new List<Vector3>();
    private const int smoothFactor = 6;

    void DrawLineRenderer(Vector3[] positions)
    {
        _displayLineRenderer.positionCount = positions.Length;
        _displayLineRenderer.SetPositions(positions);
    }


    private void BuildPoints()
    {
        vertexs.Clear();
        foreach (var i in currentIndexes) vertexs.Add(letterPositions[i]);

        // if (currentIndexes.Count == 1 || vertexs.Count >= 1 && Vector3.Distance(InputPosition, vertexs[vertexs.Count - 1]) >= _radius)
        {
            vertexs.Add(InputPosition);
        }
    }

    private int GetNearestPosition(Vector3 point, List<Vector3> letters)
    {
        float min = float.MaxValue;
        int index = -1;
        for (int i = 0; i < letters.Count; i++)
        {
            float distant = Vector3.Distance(point, letters[i]);
            if (distant < min)
            {
                min = distant;
                index = i;
            }
        }
        return index;
    }


    private Vector3 InputPosition
    {
        get
        {
            var mousePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePoint.z = 90;
            return mousePoint;
        }
    }
}
