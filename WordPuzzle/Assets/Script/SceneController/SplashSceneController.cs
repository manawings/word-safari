﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SplashSceneController : MonoBehaviour {
    [SerializeField] private Sprite _soundOn;
    [SerializeField] private Sprite _soundOff;
    [SerializeField] private Sprite _musicOn;
    [SerializeField] private Sprite _musicOff;
    [SerializeField] private Sprite _languageEN;
    [SerializeField] private Sprite _languageTH;
    [SerializeField] private Sprite _titleTH;
    [SerializeField] private Sprite _titleEN;


    [SerializeField] private Image _soundImage;
    [SerializeField] private Image _musicImage;
    [SerializeField] private Image _languageImage;
    [SerializeField] private Image _titleImage;
    [SerializeField] private Text _playText;
    [SerializeField] private Text _titleText;

    private string _nextScene; 

    void Start()
    {
        _nextScene = Profile.Instance.hasLoadData ? "Game" : "Mode";
        _soundImage.sprite = SoundManager.instance.IsSoundEnabled() ? _soundOn : _soundOff;
        _musicImage.sprite = SoundManager.instance.IsMusicEnabled() ? _musicOn : _musicOff;
        UpdateLanguage();
    }


    void UpdateLanguage ()
    {
        if (LanguageManager.instance.CurrentLanguage == LanguageManager.Language.EN.ToString())
        {
            _playText.text = "PLAY";
            _titleText.text = "Word Puzzle";
            _titleImage.sprite = _titleEN;
            _languageImage.sprite = _languageEN;
         }
        else
        {
            _playText.text = "เล่น";
            _titleText.text = "เกมส์ทายคำ";
            _titleImage.sprite = _titleTH;
            _languageImage.sprite = _languageTH;

        }


    }

    public void Play()
    {
        SceneManager.LoadScene(_nextScene);
        SoundManager.instance.PlayButtonSound();
        FireBaseManager.instance.LogPlay();
    }

    public void ToogleSound()
    {
        bool enable = !SoundManager.instance.IsSoundEnabled();
        SoundManager.instance.SetSoundEnabled(enable);
        _soundImage.sprite = enable ? _soundOn : _soundOff;
        SoundManager.instance.UpdateSetting();
        FireBaseManager.instance.LogSoundToogle();

    }

    public void ToogleMusic()
    {
        bool enable = !SoundManager.instance.IsMusicEnabled();
        SoundManager.instance.SetMusicEnabled(enable);
        _musicImage.sprite = enable ? _musicOn : _musicOff;
        SoundManager.instance.UpdateSetting();
        FireBaseManager.instance.LogMusicToogle();

    }

    public void ToogleLanguage ()
    {
        LanguageManager.Language language = LanguageManager.Language.EN;
        if (LanguageManager.instance.CurrentLanguage == LanguageManager.Language.EN.ToString())
        {
            language = LanguageManager.Language.TH;
        }
        LanguageManager.instance.SetLanguage(language);
        UpdateLanguage();
        FireBaseManager.instance.LogLangaugeToogle();
    }
}
