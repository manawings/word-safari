﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ModeSceneController : MonoBehaviour {

    [SerializeField] private Mode _modePrefab;
    [SerializeField] private HeaderBarController _headerBarController;
    [SerializeField] private Mode[] mainLevels;
    [SerializeField] private GameObject _parent;
    private const float GAP_Y = -300;

    void Start ()
    {

        for (int i = 0; i < LevelManager.instance.allLevels.Count; i++)
        {
            CreateMode(LevelManager.instance.allLevels[i], new Vector3(0, 700f + (i * GAP_Y)));
        }
        _headerBarController.Calibrate(BackToSplash, false);


    }

    private void CreateMode(LevelData leveldata, Vector3 positioVector3)
    {
        var newMode = Instantiate<Mode>(_modePrefab, _parent.transform);
        Debug.Log(positioVector3);
        newMode.transform.localPosition = positioVector3;
        newMode.Calibrate(leveldata);
        Button button = newMode.GetComponent<Button>();
        button.interactable = (newMode.levelData.index <= LevelManager.instance.MaxProgress);
    }

    void BackToSplash()
    {
        SceneManager.LoadScene("Splash");
    }

}
