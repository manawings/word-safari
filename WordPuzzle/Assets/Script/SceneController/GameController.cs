﻿using System;
using UnityEngine;
using K2.Popup;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private WordCircleView _wordCircleView;
    [SerializeField] private AnswerController answerController;
    [SerializeField] char[] alphabets;
    [SerializeField] private HeaderBarController _headerBarController;
    [SerializeField] private GameAnimationController _gameAnimationController;
    [SerializeField] private Compliment _compliment;
    [SerializeField] private ParticleSystem _particleSystem;


    private const float CHECK_ANSWER_TIME = 0.25f;
    private const float END_GAME_DELAY = 0.5f;
    private string[] thisLevelStrings;

    private void Start ()
	{
        _wordCircleView.gameController = this;
        Calibrate();

    }

    void OnApplicationQuit()
    {
        Profile.Save();
    }

    private void Calibrate()
    {
        int level = Profile.Instance.CurrentLevelData.currentLevel;
        //    thisLevelStrings = Profile.Instance.SubLevelList[level];
        thisLevelStrings = Profile.Instance.CurrentLevelData.SubLevelList[level];

        alphabets = thisLevelStrings[1].ToCharArray();
        _wordCircleView.Calibrate(alphabets);
        _headerBarController.Calibrate(BackToMenu, true);
        GenerateAnswer(thisLevelStrings);
        PutCurrentPlayWords();
    }

    void PutCurrentPlayWords()
    {
        if (global::Profile.Instance.CurrentPlayedWords == null || global::Profile.Instance.CurrentPlayedWords.Count == 0) return;
        for (int i = 0; i < global::Profile.Instance.CurrentPlayedWords.Count; i++)
            answerController.CheckAnswer(global::Profile.Instance.CurrentPlayedWords[i]);
    }

    void BackToMenu()
    {
        SceneManager.LoadScene("Mode");
        Profile.Save();
    }

    private void GenerateAnswer(string[] alphabetStrings)
    {
        int extraWordLength = int.Parse(alphabetStrings[0]);
        string[] answersString = new string[alphabetStrings.Length - extraWordLength - 2];
        string[] extraWordString = new string[extraWordLength];

        for (int i = 0; i < answersString.Length; i++)
            answersString[i] = alphabetStrings[i + 2];

        for (int i = 0; i < extraWordLength; i++)
        {
            extraWordString[i] = alphabetStrings[answersString.Length + i + 2];
            Debug.Log(extraWordString[i]);
        }

        answerController.Calibrate(answersString, alphabets.Length,extraWordString);
    }

    public Color CheckAnswer(string selectedString)
    {
        K2.Time.Add(Reset, delayInSeconds: CHECK_ANSWER_TIME, repeatCount: 1);
        var isWordValid = answerController.CheckAnswer(selectedString.ToLower());
        var isExtraWordValid = answerController.CheckExtraAnswer(selectedString.ToLower());

        if (isExtraWordValid)
        {
            SoundManager.instance.PlayMatchSound();
            _headerBarController.DeployExtraWordBonus(selectedString);
            return Color.blue;
        }

        if (!isWordValid)
            return Color.red;

        if (!answerController.IsLevelComplete())
        {
            ///Answer that already done
            if (Profile.Instance.CurrentPlayedWords.Contains(selectedString))
                return Color.yellow;

            ///Correct answer
            SoundManager.instance.PlayMatchSound();
            _particleSystem.Play();
            Profile.Instance.CurrentPlayedWords.Add(selectedString);
            _compliment.Show(Compliment.Type.GoodJob);
     //       _gameAnimationController.DeployTextPopup("CORRECT !!!");
            return Color.green;
        }

        ///Complete Level
        SoundManager.instance.PlayMatchSound();
        EndLevel();
        return Color.magenta;
    }


    private void EndLevel()
    {
        InputController.Lock();
        _compliment.Show(Compliment.Type.WellDone);
        K2.Time.Add(End, END_GAME_DELAY, 1);   
    }

    void End()
    {
        InputController.Unlock();
        SoundManager.instance.PlayWinSound();
        _wordCircleView._label.text = "Completed";
        DeployPopUpEnd();
        LevelManager.instance.Progress();
    }

    private void DeployPopUpEnd()
    {
        _headerBarController.DeployPopUpEnd(Profile.Instance.CurrentLevelData.SubLevelList.Count, Profile.Instance.CurrentLevelData.index, Profile.Instance.CurrentLevelData.currentLevel);
    }

    private void Reset()
    {
        _wordCircleView.Reset();
    }

    public void Hint()
    {
        SoundManager.instance.PlayButtonSound();
        if (Profile.Instance.Gold < Profile.HINT_COST)
        {
            _headerBarController.DeployError("Not enought gold to hint.");
        }
        else
        {
            answerController.Hint();
            _headerBarController.UpdateGold();
            if (answerController.IsLevelComplete())
            {
                EndLevel();
            }
        }
        FireBaseManager.instance.LogHint();
    }


}
