﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Level _levelPrefab;
    [SerializeField] private HeaderBarController _headerBarController;
    [SerializeField] private GameObject _holder;
    [SerializeField] private Image _theme;

    const float GAP_Y = 150f;
    const float Scale = 0.33f;
    void Start()
    {
        if(_theme != null)
             _theme.sprite = Profile.Instance.CurrentLevelData.nameSprite;
        var contentHeight = Profile.Instance.CurrentLevelData.SubLevelList.Count * GAP_Y;

        var beginY = (contentHeight / 2f) - (GAP_Y /2f) - (GAP_Y * Scale);
        for (int i = 0; i < Profile.Instance.CurrentLevelData.SubLevelList.Count; i++)
        {
              CreatLevel(i, Profile.Instance.CurrentLevelData.maxLevel >= i, beginY);
        }
        _holder.GetComponent<RectTransform>().sizeDelta = new Vector2(1000f, contentHeight);
        _headerBarController.Calibrate(BackToMode, false);
    }

    private void CreatLevel(int index,bool interactable, float beginY)
    {
        var newLevel = Instantiate<Level>(_levelPrefab, _holder.transform);
        newLevel.Calibrate(index,interactable,beginY,GAP_Y);
    }

    public void PlayGame(int index)
    {
        if (Profile.Instance.CurrentLevelData.currentLevel != index)
        {
            Profile.Instance.CurrentPlayedWords.Clear();
            Profile.Instance.CurrentLevelData.currentLevel = index;
        }
        SceneManager.LoadScene(1);
    }

    void BackToMode()
    {
        SceneManager.LoadScene("Mode");
    }
}
