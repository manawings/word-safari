﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    [SerializeField] Sprite[] iconsNormal;
    [SerializeField] Sprite[] iconsActive;
    [SerializeField] Sprite[] titles;
    [SerializeField] TextAsset[] csvs;
    public List<LevelData> allLevels = new List<LevelData>();
    private int _maxProgress;
    public static LevelManager instance = null;

    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
        {
            //if not, set it to this.
            instance = this;
        }
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    public void Load()
    {
        CreateLevel();
    }


    void CreateLevel()
    {
        for (int i = 0; i < csvs.Length; i++)
        {
            var newLevelData = new LevelData();
            newLevelData.Calibrate(csvs[i], iconsNormal[i], iconsActive[i], titles[i], i, false, 0);
            allLevels.Add(newLevelData);
        }
    }

    public void SetMaxProgress(int maxMode, int maxLevel)
    {
        for (int i = 0; i < allLevels.Count; i++)
        {
            if (i < maxMode)
                allLevels[i].Complete();
            else
            {
                allLevels[i].maxLevel = maxLevel;
                _maxProgress = i;
            }
        }
    }

    public LevelData GetMainLevel(int index)
    {
        return allLevels[index];
    }

    public int MaxProgress {
        get
        {
            if (_maxProgress > allLevels.Count)
                _maxProgress = allLevels.Count;
            return _maxProgress;
        }
    }

    public int MaxSubProgress {
        get
        {
            return allLevels[MaxProgress].maxLevel;
        }
    }

    void UnlockNextMainLevel (int index)
    {
        index++;
        allLevels[index].maxLevel = 0;
        Profile.Instance.CurrentLevelData = allLevels[index];
    }

    public void Progress ()
    {
        Profile.Instance.CurrentLevelData.Progress();
        if(Profile.Instance.CurrentLevelData.HasClear)
        {
            UnlockNextMainLevel(Profile.Instance.CurrentLevelData.index);
            _maxProgress++;
        }

        Profile.Instance.CurrentPlayedWords.Clear();






        //Profile.Instance.CurrentSubLevel++;
        //if (Profile.Instance.CurrentSubLevel > Profile.Instance.MaxSubLevel)
        //{
        //    Profile.Instance.MaxSubLevel++;
        //    Profile.Instance.CurrentLevelData.maxLevel++;
        //}
        //if (Profile.Instance.MaxSubLevel >= Profile.Instance.CurrentLevelData.SubLevelList.Count)
        //{
        //    Profile.Instance.MaxMainLevel++;
        //    Profile.Instance.CurrentMainLevel++;
        //    Profile.Instance.MaxSubLevel = 0;
        //    Profile.Instance.CurrentSubLevel = 0;
        //    Profile.Instance.CurrentLevelData = GetMainLevel(Profile.Instance.MaxMainLevel);
        //}// unlock new main lv
        //LevelManager.instance.SetMaxProgress(Profile.Instance.MaxMainLevel, Profile.Instance.MaxSubLevel);

        //Profile.Instance.CurrentPlayedWords.Clear();
    }
}
