﻿using K2.Popup;
using UnityEngine;
using UnityEngine.UI;

public class ErrorPopup : PopupContent
{
    [SerializeField] private Text bodyText;

    public void Deploy(string text)
    {
        bodyText.text = text;
    }

    public void Close()
    {
        PopupSystem.Instance.CloseLayer();
    }
}
