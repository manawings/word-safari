﻿using K2.Popup;
using UnityEngine;
using UnityEngine.UI;


public class ExtraWordPopup : PopupContent
{

    [SerializeField] private Text bodyText;

    public ExtraWordPopup CloneAndDeploy(int gold)
    {
        var clone = Instantiate(this);
        clone.Deploy(gold);
        Profile.Instance.Gold += gold;
        return clone;
    }

    public void Deploy(int gold)
    {
        bodyText.text = string.Format("You found a bonus.\nGain {0} gold.", gold);
    }

    public void Continue()
    {
        PopupSystem.Instance.CloseLayer();
    }
}
