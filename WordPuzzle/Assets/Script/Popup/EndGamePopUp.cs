﻿using  UnityEngine.UI;
using K2.Popup;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGamePopUp : PopupContent
{
    [SerializeField] Sprite[] _modes;
    [SerializeField] Sprite[] _levels;
    [SerializeField] Image _mode;
    [SerializeField] Image _level;
    
    private string _nextScene = "Undefined";

    public EndGamePopUp CloneAndDeploy(string nextScene, int gold, int mode, int level)
    {
        var clone = Instantiate(this);
        clone.Deploy(nextScene, gold,mode,level);
        return clone;
    }

    public void Deploy(string nextScene, int gold,int mode, int level)
    {
        _mode.sprite = _modes[mode];
        _level.sprite = _levels[level];
        _nextScene = nextScene;
        Debug.Log("");
    }

    public void Continue()
    {
        Profile.Instance.Gold += Profile.GOLD_PER_LEVEL;
        SceneManager.LoadScene(_nextScene);
    }
}
