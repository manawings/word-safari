﻿using K2.Popup;
using UnityEngine;

public class IAPPopup : PopupContent
{
    public void Close()
    {
        PopupSystem.Instance.CloseLayer();
    }
}
