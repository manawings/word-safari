﻿using System.Collections.Generic;
using UnityEngine;

public class WordView : MonoBehaviour
{
    private const float GapPerAlphabet = 100f;
    private List<LetterView> alphabets = new List<LetterView>();
    [SerializeField] private LetterView _letterViewPrefab;

    public WordView()
    {
        IsComplete = false;
    }

    public bool IsComplete { get; set; }

    public void Calibate(string word, Vector3 wordPosition, float scale)
    {
        IsComplete = false;
        var wordChar = word.ToCharArray();
       
        transform.localPosition = wordPosition;
        transform.localScale = new Vector3(scale, scale, scale);

        for (int i = 0, m = word.Length; i < m; i++)
            CreateLetterView(i, wordChar);
    }

    private void CreateLetterView(int i, char[] wordChar)
    {
        var pos = new Vector3(i * GapPerAlphabet, 0, 0);
        var newAlphabet = Instantiate<LetterView>(_letterViewPrefab, transform);
        newAlphabet.transform.localPosition = pos;
        newAlphabet.Calibrate(wordChar[i]);
        alphabets.Add(newAlphabet);
    }

    public void RevealWord()
    {
        IsComplete = true;
        for (int i = 0; i < alphabets.Count; i++)
        {
          //  K2.Time.Add(alphabets[i].Reveal, 0.4f * i, 1);
           alphabets[i].Reveal();
        }
      //  foreach (var answerAlphabet in alphabets)
    }

    public void Hint(int letterIndex)
    {
        alphabets[letterIndex].Reveal();
    }

    public int Count
    {
        get { return alphabets.Count; }
    }
    
}