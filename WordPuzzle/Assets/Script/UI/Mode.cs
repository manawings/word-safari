﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Mode : MonoBehaviour
{
    [SerializeField] private Text text;
    private TextAsset _textAsset;
    [SerializeField] private Image icon;

    public int MainLevel = 0;
    public LevelData levelData;

    public void Calibrate(LevelData levelData)
    {
        this.levelData = levelData;
        name = levelData.Asset.name;
        text.text = name;
        icon.sprite = levelData.iconSprite;
        _textAsset = levelData.Asset;
    }

    public void GotoLevel()
    {
        SoundManager.instance.PlayButtonSound();
        icon.sprite = levelData.iconSpriteActive;

        //  if (Profile.Instance.Asset != _textAsset)
        if (Profile.Instance.CurrentLevelData != levelData)
         {
                Profile.Instance.CurrentPlayedWords.Clear();
          //  Profile.Instance.Asset = _textAsset;
            Profile.Instance.CurrentLevelData = levelData;
           // Profile.Instance.CurrentMainLevel = MainLevel;
        }
        SceneManager.LoadScene("Menu");
        FireBaseManager.instance.LogModeSelect(levelData.index);
    }
}
