﻿using System;
using K2.Popup;
using UnityEngine;
using UnityEngine.UI;

public class HeaderBarController : MonoBehaviour
{
    [SerializeField] private Button navigatorButton;
    [SerializeField] private Image levelText;
    [SerializeField] private GameObject levelTextHolder;

    [SerializeField] private Button currentcyButton;

    [SerializeField] private PausePopup pausePopup;
    [SerializeField] private IAPPopup iapPopup;
    [SerializeField] private EndGamePopUp _endGamePopUpPrefab;
    [SerializeField] private ExtraWordPopup _extraWordPopUpPrefab;

    [SerializeField] private ErrorPopup errorPopUp;
    [SerializeField] Sprite[] _levels;

    public event Action OnNavigator;


    public void Calibrate(Action OnNavigator, bool isGameScene = false)
    {
        navigatorButton.GetComponentInChildren<Text>().text = "Back";
        if (isGameScene)
        {
            levelText.sprite = _levels[Profile.Instance.CurrentLevelData.currentLevel];
            levelTextHolder.gameObject.SetActive(true);
        }
        else
            levelTextHolder.gameObject.SetActive(false);
        UpdateGold();
        this.OnNavigator = OnNavigator;
        if (this.OnNavigator == null)
        {
            navigatorButton.gameObject.SetActive(this.OnNavigator !=null);
        }
    }

    public void OnNavigation ()
    {
        SoundManager.instance.PlayButtonSound();
        if (OnNavigator != null)
            OnNavigator();
        FireBaseManager.instance.LogBack();

    }

    public void OnBuyCurrency()
    {
        SoundManager.instance.PlayButtonSound();
        PopupSystem.Instance.Show();
        var content = Instantiate(iapPopup);
        PopupSystem.Instance.Add(content);
        FireBaseManager.instance.LogIAPPopup();

    }

    public void DeployPopUpEnd(int levelCount,int mainLevel,int subLevel)
    {
        var nextScene = Profile.Instance.CurrentLevelData.HasClear ? "Menu" : "Game";
        // EndGamePopUp.Deploy(nextScene, 9999);
        var popup = _endGamePopUpPrefab.CloneAndDeploy(nextScene, Profile.GOLD_PER_LEVEL, mainLevel, subLevel);
        PopupSystem.Instance.Show();
        PopupSystem.Instance.Add(popup);
    }


    public void DeployExtraWordBonus(string word)
    {
        var popup = _extraWordPopUpPrefab.CloneAndDeploy(Profile.EXTRA_WORD_BONUS);
        PopupSystem.Instance.Show();
        PopupSystem.Instance.Add(popup);
        UpdateGold();
    }

    public void UpdateGold()
    {
        currentcyButton.GetComponentInChildren<Text>().text =  "" + Profile.Instance.Gold;
    }

    public void DeployError(string text)
    {
        errorPopUp.Deploy(text);
        PopupSystem.Instance.Show();
        var content = Instantiate(errorPopUp);
        PopupSystem.Instance.Add(content);
    }
}
