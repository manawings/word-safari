﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour {
    [SerializeField] private Image _level;
    [SerializeField] private Image _status;
    [SerializeField] private Image _badge;

    [SerializeField] private Button _button;
    private int _index;
  //  private const float GAP_Y = -150f;
    [SerializeField] Sprite[] _levelActive;
    [SerializeField] Sprite[] _levelInActive;
    [SerializeField] Sprite _solve;
    [SerializeField] Sprite _unSolve;
    [SerializeField] Sprite _lock;
    [SerializeField] Sprite _unlock;



    public void Calibrate(int index, bool interactable,float beginY,float gapY)
    {
        _index = index;
     //   _text.text = "LEVEL " + (index + 1);
        _button.interactable = interactable;
        if (interactable)
        {
            _level.sprite = _levelActive[index];
            _status.sprite = _solve;
            _badge.sprite = _unlock;
        }
        else
        {
            _level.sprite = _levelInActive[index];
            _status.sprite = _unSolve;
            _badge.sprite = _lock;

        }
        transform.localPosition = new Vector3(0,beginY + (_index * -gapY),0);

    }

    public void GotoLevel()
    {
        Profile.Instance.CurrentLevelData.currentLevel = _index;
        SceneManager.LoadScene("Game");
        FireBaseManager.instance.LogModeSelect(_index);
    }
}
