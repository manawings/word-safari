﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script.UI
{
    public class Curver 
    {
        //arrayToCurve is original Vector3 array, smoothness is the number of interpolations. 
        public static Vector3[] MakeSmoothCurve(Vector3[] arrayToCurve, float smoothness)
        {
            var pointsLength = 0;
            var curvedLength = 0;

            if (smoothness < 1.0f) smoothness = 1.0f;

            pointsLength = arrayToCurve.Length;

            curvedLength = (pointsLength * Mathf.RoundToInt(smoothness)) - 1;
            var curvedPoints = new List<Vector3>(curvedLength);

            for (var pointInTimeOnCurve = 0; pointInTimeOnCurve < curvedLength + 1; pointInTimeOnCurve++)
            {
                var t = Mathf.InverseLerp(0, curvedLength, pointInTimeOnCurve);

                var points = new List<Vector3>(arrayToCurve);
                for (var j = pointsLength - 1; j > 0; j--)
                {
                    for (var i = 0; i < j; i++)
                    {
                        points[i] = (1 - t) * points[i] + t * points[i + 1];
                    }
                }

                curvedPoints.Add(points[0]);
            }
            return (curvedPoints.ToArray());
        }
    }
}
