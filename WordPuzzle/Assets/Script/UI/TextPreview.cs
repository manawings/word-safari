﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class TextPreview : MonoBehaviour {
    public GameObject content;

    public Image Background;
    public Text text;

    private char[] _letters;
    public Color defaultColor;

    public void SetIndexes(List<int> indexes)
    {
        StringBuilder sb = new StringBuilder();
        foreach(var i in indexes)
        {
            sb.Append(_letters[i]);
        }
        text.text = sb.ToString();
        Background.rectTransform.sizeDelta = new Vector2(text.preferredWidth + 50, Background.rectTransform.sizeDelta.y);
    }

    public void SetActive(bool isActive)
    {
        content.SetActive(isActive && text.text.Length > 0);
    }

    public void ClearText()
    {
        text.text = "";
    }

    public void SetLetter(char[] letters)
    {
        _letters = letters;
    }

    public void SetText(string textStr)
    {
        text.text = textStr;
    }

    public string GetText()
    {
        return text.text;
    }

    public void SetDefaultColor()
    {
        Background.color = defaultColor;
    }

    public  void SetColor (Color color)
    {
        Background.color = color;
    }


    public void FadeOut()
    {
        ClearText();
        iTween.ValueTo(gameObject, iTween.Hash("from", 1, "to", 0, "time", 0.4f, "onupdate", "OnUpdate", "ontarget", gameObject));
    }

    public void FadeIn()
    {
        iTween.Stop(gameObject);
        SetDefaultColor();
    }

    private void OnUpdate(float value)
    {
        Background.color = new Color(Background.color.r, Background.color.g, Background.color.b, value);
    }

}
