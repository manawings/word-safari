﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Line : MonoBehaviour {

	// Use this for initialization
	[SerializeField] Image _lineImage;
    [SerializeField] private Color _originColor;
	Vector2 _originPosition;
    private const float LINE_TIGHT = 20f;
    private const float REFERENCE_HIGHT = 1136f;
    public float angle;
    public event Action OnBeginDrawLine;
    public event Action OnEndDrawLine;



    public void SetOriginPosition (Vector2 originPosition)
    {
        _originPosition = originPosition;
    }

    public void DrawLine(Vector2 differenceVector)
    {
        // differenceVector = MaptoScreenSize(differenceVector);
        if(OnBeginDrawLine != null) OnBeginDrawLine();
        _lineImage.rectTransform.sizeDelta = new Vector2(differenceVector.magnitude * FactorHeight, LINE_TIGHT);
        _lineImage.rectTransform.pivot = new Vector2(0, 0.5f);
        _lineImage.rectTransform.position = _originPosition;
        angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
        _lineImage.rectTransform.rotation = Quaternion.Euler(0, 0, angle);
        if (OnEndDrawLine != null) OnEndDrawLine();
    }



    private float FactorHeight
    {
        get
        {
            return REFERENCE_HIGHT / (float) Screen.height;
        }
    }
   

    public void Reset()
    {
        _lineImage.color = _originColor;
        _lineImage.rectTransform.sizeDelta = new Vector2(0, 0);
    }

    public void SetColor(Color color)
    {
        _lineImage.color = color;
    }
}
