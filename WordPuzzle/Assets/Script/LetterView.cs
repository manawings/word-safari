﻿using System;
using UnityEngine;
using  UnityEngine.UI;

public class LetterView : MonoBehaviour
{
    private char value;
   // [SerializeField] private Text label;
    [SerializeField] private Image _letterImage;

    public event Action<float> OnReveal;

    public LetterView()
    {
        IsRevealed = false;
        OnReveal += Animate;
    }

    public bool IsRevealed { get; set; }

    public void Calibrate(char value)
    {
        this.value = value;
        IsRevealed = false;
        _letterImage.sprite = Resources.Load<Sprite>("Font/" + value);
        _letterImage.gameObject.SetActive(false);
    }

    public void Reveal(float delay = 0f)
    {
        if (OnReveal != null) OnReveal(delay);
        IsRevealed = true;
        _letterImage.gameObject.SetActive(true);
    }

    void Animate(float delay)
    {
        LeanTween.scale(this.gameObject, new Vector3(1.5f, 1.5f, 1.0f), 0.2f).setDelay(delay);
        LeanTween.scale(this.gameObject, new Vector3(1.0f, 1.0f, 1.0f), 0.2f).setDelay(delay + 0.2f);
    }
}
