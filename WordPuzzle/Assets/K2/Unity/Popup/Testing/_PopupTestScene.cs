﻿using UnityEngine;

namespace K2.Popup
{
    public class _PopupTestScene : MonoBehaviour
    {

        [SerializeField] private PopupContent _sampleContent;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                CreateNewLayer();

            if (Input.GetKeyDown(KeyCode.Escape))
                CloseLayer();

            if (Input.GetKeyDown(KeyCode.RightArrow))
                AddContent();

            if (Input.GetKeyDown(KeyCode.LeftArrow))
                BackContent();
        }

        private void AddContent()
        {
            var content = Instantiate(_sampleContent);
            PopupSystem.Instance.Add(content);
        }

        private void BackContent()
        {
            PopupSystem.Instance.Back();
        }

        private void CreateNewLayer()
        {
            PopupSystem.Instance.Show();
        }

        private void CloseLayer()
        {
            PopupSystem.Instance.CloseLayer();
        }
    }
}

