﻿using System.Collections.Generic;
using UnityEngine;

namespace K2.Popup
{
    public class PopupSystem : MonoBehaviour
    {

        [SerializeField] private PopupLayer _popupLayerPrefab;

        private List<PopupLayer> _layers;

        private void Awake()
        {
            _Instance = this;
        }

        public PopupSystem()
        {
            _layers = new List<PopupLayer>();
        }

        private static PopupSystem _Instance;

        public static PopupSystem Instance
        {
            get
            {
                if (_Instance != null)
                    return _Instance;

                var gameObject = new GameObject("K2: Popup System");
                _Instance = gameObject.AddComponent<PopupSystem>();
                gameObject.transform.SetAsLastSibling();
                DontDestroyOnLoad(gameObject);
                return _Instance;
            }
        }

        public void Show()
        {
            AddNewLayer();
        }

        public void Add(PopupContent content)
        {
            if (CurrentLayer == null)
                AddNewLayer();

            CurrentLayer.Display(content);
        }

        public PopupLayer AddNewLayer()
        {
            var layer = Instantiate(_popupLayerPrefab);
            layer.gameObject.name = string.Format("Layer {0}", _layers.Count + 1);
            layer.transform.SetParent(transform, false);
            _layers.Add(layer);
            return layer;
        }

        public void Back()
        {
            CurrentLayer.Back();
            if (CurrentLayer.IsEmpty)
                CloseLayer();
        }

        public void CloseLayer()
        {
            var layerIndex = _layers.Count - 1;
            _layers[layerIndex].Hide();
            _layers.RemoveAt(layerIndex);
        }

        public void CloseAll()
        {

        }

        public PopupLayer CurrentLayer
        {
            get { return _layers.Count == 0 ? null : _layers[_layers.Count - 1]; }
        }
    }
}
