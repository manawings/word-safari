﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace K2.Unity
{
    public class SceneTransitioner : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private string _fadeInState, _fadeOutState;

        private string _sceneName;

        public void Initialize(string sceneName)
        {
            _sceneName = sceneName;
            DontDestroyOnLoad(this);
            FadeIn();
        }

        private void FadeIn()
        {
            _animator.Play(_fadeInState);
        }

        private void FadeOut(Scene arg0, Scene arg1)
        {
            _animator.Play(_fadeOutState);
        }

        public void ExecuteTransition()
        {
            SceneManager.activeSceneChanged += FadeOut;
            SceneManager.LoadScene(_sceneName);
        }

        public void OnTransitionComplete()
        {
            SceneManager.activeSceneChanged -= FadeOut;
            Destroy(gameObject);
        }
    }
}