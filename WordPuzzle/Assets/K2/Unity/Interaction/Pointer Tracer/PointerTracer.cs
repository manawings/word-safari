﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PointerTracer : MonoBehaviour
{
    private IList<Vector2> _tracePositions;
    private bool _isTracing;

    public Canvas canvas;
    public GameObject traceTrailPrefab;
    public GameObject traceLeader;

    public void Update()
    {
        // TODO: This should be a state machine.

        if (Input.GetMouseButtonDown(0))
            StartTrace();

        if (Input.GetMouseButtonUp(0))
            StopTrace();

        if (_isTracing)
            UpdateTracer(GetTracerPosition());
    }

    private Vector2 GetTracerPosition()
    {
        var x = (Input.mousePosition.x - Screen.width * 0.5f) / canvas.scaleFactor;
        var y = (Input.mousePosition.y - Screen.height * 0.5f) / canvas.scaleFactor;
        return new Vector2(x, y);
    }

    private void StartTrace()
    {
        _tracePositions = new List<Vector2>();
        _isTracing = true;
    }

    private void StopTrace()
    {
        _isTracing = false;
    }

    private void UpdateTracer(Vector2 newPosition)
    {
        _tracePositions.Add(newPosition);
        DrawTraceTrail(newPosition);
        traceLeader.transform.localPosition = newPosition;
    }

    private void DrawTraceTrail(Vector2 position)
    {
        var trail = Instantiate(traceTrailPrefab);
        trail.transform.SetParent(transform, false);
        trail.transform.localPosition = position;
    }
}
