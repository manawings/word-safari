﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

/// <summary>
/// Parses an input CSV string into one of three possible formats:
/// 1. A list of (string[]) values, with an int index.
/// 2. A list of (Dictionary) values, indexed by a key.
/// 3. A 2D dictionary, indexed by two string keys.
/// </summary>
public class CSVParser
{
    private const string KSplitRegex = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
    private const string KLineSplitRegex = @"\r\n|\n\r|\n|\r";

    public static Dictionary<string, Dictionary<string, string>> ReadAs2DDictionary(string csvString)
    {
        var dictionary2D = new Dictionary<string, Dictionary<string, string>>();
        var list = ReadAsList(csvString);

        if (list.Count < 2)
            throw new InvalidCSVInput("Must have at least 2 rows of values (column keys, column value).");

        if (list[0].Length < 2)
            throw new InvalidCSVInput("Must have at least 2 columns of values (row keys, row value).");

        var keys = list[0].ToArray();
        for(int i = 1, m = list.Count ; i < m ; i++)
        {
            var key1 = list[i][0];
            var rowValues = new Dictionary<string, string>();
            dictionary2D.Add(key1, rowValues);

            for (int j = 1, m2 = keys.Length; j < m2; j++)
            {
                var key2 = keys[j];
                var value = list[i][j];
                rowValues.Add(key2, value);
            }
        }
        return dictionary2D;
    }

    public static IList<string> ReadAsList1D(string csvString)
    {
        var list = new List<string>();
        ExecuteActionOnRow(csvString, (i, x) => list.Add(string.Join("", x)));
        return list;
    }

    public static IList<string[]> ReadAsList(string csvString)
    {
        var list = new List<string[]>();
        ExecuteActionOnRow(csvString, (i, x) => list.Add(x));
        return list;
    }

    public static IList<Dictionary<string, string>> ReadAsListWithKeys(string csvString)
    {
        var listWithKeys = new List<Dictionary<string, string>>();
        var lines = SplitStringIntoLines(csvString);

        if (lines.Length < 1)
            throw new InvalidCSVInput("Must have at least 1 row of values (column keys).");

        var keys = SplitLineIntoValues(lines[0]);
        ExecuteActionOnRow(csvString, (i, x) =>
        {
            if (i <= 0) return;
            var rowDictionary = new Dictionary<string, string>();
            for (int j = 0, m = keys.Length; j < m; j++)
            {
                rowDictionary.Add(keys[j], x[j]);
            }

            listWithKeys.Add(rowDictionary);
        });
        return listWithKeys;
    }

    private static void ExecuteActionOnRow(string csvString, Action<int, string[]> action)
    {
        var unsplitLines = SplitStringIntoLines(csvString);
        for (int i = 0, m = unsplitLines.Length; i < m; i++)
        {
            var rowValues = SplitLineIntoValues(unsplitLines[i]);
            action(i, rowValues);
        }
    }

    private static string[] SplitStringIntoLines(string csvString)
    {
        return Regex.Split(csvString, KLineSplitRegex);
    }

    private static string[] SplitLineIntoValues(string line)
    {
        return Regex.Split(line, KSplitRegex);
    }

    internal class InvalidCSVInput : Exception
    {
        private const string KDescription = @"The CSV input string is invalid and cannot be parsed";
        public InvalidCSVInput(string message)
            : base(string.Format(KDescription + ": {0}", message))
        {

        }
    }
}