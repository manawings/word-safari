﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace K2
{
	/// <summary>
	/// A loot bag for a generic type T. Items (T) can be added to the bag, with a weight. 
	/// Items can also be added into a weighted category.
	/// The items can be randomly fetched according to the category or weights.
	/// </summary>
	public class Loot <T>
	{

		bool isCalibrated = false;
		List <LootObject> lootList = new List<LootObject> ();
		Dictionary <string, LootCategory> categoryDict = new Dictionary<string, LootCategory> ();
		const string K_DEFAULT_CATEGORY = "Default";

		/// <summary>
		/// Add a new item category. 
		/// These categories have a weight which normalize each item's own weight calculations.
		/// The higher the weight, the more probability of this category being selected.
		/// </summary>
		/// <param name="categoryTag">Category tag.</param>
		/// <param name="weight">Weight.</param>
		public void AddCategory (string categoryTag, float weight = 1f)
		{
			if (!categoryDict.ContainsKey (categoryTag)) {
				LootCategory lootCategory = new LootCategory ();
				lootCategory.categoryTag = categoryTag;
				lootCategory.weight = weight;
				categoryDict.Add (lootCategory.categoryTag, lootCategory);
			}

			isCalibrated = false;
		}

		/// <summary>
		/// Add a new item to the specified category. 
		/// If the category does not exist, it will be created with the default weight.
		/// The item's weight determines its probabilty in comparison to other items in the same category.
		/// </summary>
		/// <param name="item">Item.</param>
		/// <param name="categoryTag">Category tag.</param>
		/// <param name="weight">Weight.</param>
		public void AddItem (T item, string categoryTag, float weight = 1f)
		{
			if (!categoryDict.ContainsKey (categoryTag)) {
				AddCategory (categoryTag);
			}

			LootObject lootObject = new LootObject ();
			lootObject.linkedObject = item;
			lootObject.weight = weight;
			lootObject.categoryTag = categoryTag;
			lootList.Add (lootObject);

			LootCategory lootCategory = categoryDict [categoryTag];
			lootCategory.chanceSum += lootObject.weight;
			lootCategory.itemCount ++;

			isCalibrated = false;
		}

		/// <summary>
		/// Add a new item to the default category.
		/// The item's weight determines its probabilty in comparison to other items in the same category.
		/// </summary>
		/// <param name="item">Item.</param>
		/// <param name="categoryTag">Category tag.</param>
		/// <param name="weight">Weight.</param>
		public void AddItem (T item, float weight = 1f)
		{
			AddItem (item, K_DEFAULT_CATEGORY, weight);
		}

		/// <summary>
		/// Get a random item from this loot bag, using a randomValue (0-1)
		/// as the comparison selector. This is if you want to have a seeded
		/// result from the looting.
		/// </summary>
		/// <returns>The item.</returns>
		/// <param name="randomValue">Random value.</param>
		/// <param name="removeItem">If set to <c>true</c> remove item.</param>
		public T GetItem (float randomValue, bool removeItem = false)
		{
			CalibrateList ();

			float randIndex = randomValue;
			LootObject lootObject = new LootObject ();

			for (int i = 0, m = lootList.Count; i < m; i++) {
				if (randIndex < lootList [i].maxValue) {
					lootObject = lootList [i];
					if (removeItem) 
						RemoveItem (lootObject);
					return (T) lootObject.linkedObject;
				}
			}

			return default (T);
		}

		/// <summary>
		/// Get a random item from the loot bag. If removeItem is set to true,
		/// the item will also be removed from the bag.
		/// </summary>
		/// <returns>The item.</returns>
		/// <param name="removeItem">If set to <c>true</c> remove item.</param>
		public T GetItem (bool removeItem = false)
		{
			return GetItem (Random.value, removeItem);
		}

		void RemoveItem (LootObject lootObject)
		{
			LootCategory lootCategory = categoryDict [lootObject.categoryTag];
			lootCategory.chanceSum -= lootObject.weight;
			lootCategory.itemCount --;
			lootList.Remove (lootObject);

			isCalibrated = false;
		}

		/// <summary>
		/// Calculate all the final weights of all items to be between 0 and 1.
		/// </summary>
		void CalibrateList ()
		{
			if (!isCalibrated) {
			
				isCalibrated = true;
				float chanceSum = 0;
				float previousValue = 0;

				Dictionary<string, LootCategory>.ValueCollection lootCategories = categoryDict.Values;

				foreach (LootCategory lootCategory in lootCategories) {
					// Count the items in each category. Add up the sum score.
					if (lootCategory.itemCount != 0) chanceSum += lootCategory.weight;
				}

				foreach (LootCategory lootCategory in lootCategories) {
					// Work out the roll range for each category. Ignore ones that do not have count.
					if (lootCategory.itemCount != 0) {
						previousValue = lootCategory.CalculateRange (previousValue, chanceSum);
					} else {
						lootCategory.minValue = lootCategory.maxValue = lootCategory.valueRange = 0;
					}

					lootCategory.previousValue = lootCategory.minValue;
				}

				for (int i = 0, m = lootList.Count; i < m; i++) {
					// Work out the min and max range for each item based on the category chances.
					LootObject lootObject = lootList [i];
					LootCategory lootCategory = categoryDict [lootList [i].categoryTag];
					lootCategory.previousValue = lootObject.CalculateRange (lootCategory.previousValue, lootCategory.chanceSum, lootCategory.valueRange);
				}
			}
		}

		/// <summary>
		/// Returns a list of all the loot objects currently in this bag.
		/// </summary>
		/// <returns>The loot object list.</returns>
		public List <LootObject> GetLootObjectList ()
		{
			CalibrateList ();
			return lootList;
		}

		/// <summary>
		/// Debugs a string of all the categories and items on this loot bag.
		/// </summary>
		public void DebugLootList ()
		{
			CalibrateList ();

			foreach (LootCategory category in categoryDict.Values) {
				Debug.Log (category);
			}

			foreach (LootObject lootObject in lootList) {
				Debug.Log (lootObject);
			}
		}
	}

	/// <summary>
	/// A wrapper class to hold the weight, chance and references for each item of loot.
	/// </summary>
	public class LootObject
	{

		public float weight = 1f;
		public float minValue = 0, maxValue = 1, valueRange = 1;
		public object linkedObject;
		public string categoryTag;

		public float CalculateRange (float startingValue, float chanceSum, float imposedRange = 1f)
		{
			valueRange = (weight / chanceSum) * imposedRange;
			minValue = startingValue;
			maxValue = startingValue + valueRange;
			return maxValue;
		}

		public float DropChance {
			get {
				return valueRange;
			}
		}

		public override string ToString ()
		{
			return string.Format ("[{0}] Category: {1}, Drop Chance: {2}", linkedObject.ToString (), categoryTag, string.Format ("{0}%", (DropChance * 100).ToString ("F2")));
		}
	}

	class LootCategory : LootObject
	{
		public int itemCount = 0;
		public float chanceSum = 0;
		public float previousValue = 0;

		public override string ToString ()
		{
			return string.Format ("[Category: {0}] Drop Chance: {1}, Range: {2} - {3}", categoryTag, string.Format ("{0}%", (DropChance * 100).ToString ("F2")), minValue, maxValue);
		}
	}
}