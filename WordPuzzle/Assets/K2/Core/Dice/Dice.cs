﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace K2
{
    public interface IRandomProvider
    {
        float Value();
        float Range(float from, float to);
        int Range(int from, int to);
    }

    public interface ICollectionRandomizer
    {
        T RandomMemberOf<T>(IList<T> objectCollection);
        int RandomIndexOf(IList objectCollection);
        List<T> Shuffle<T>(IEnumerable<T> original);
    }

    public interface IDice : IRandomProvider, ICollectionRandomizer
    {
        bool Roll(float chance);
        bool Roll(int chance);
    }

    public class Dice
    {
        private static Dice _Singleton;

        private bool _isSeeded;
        private float[] _seedArray;
        private int _seedIndex, _seed;

        public static Dice Singleton
        {
            get
            {
                if (_Singleton != null)
                    return _Singleton;

                _Singleton = new Dice();
                _Singleton.Initialize();
                return _Singleton;
            }
        }

        private static int PseudoRandomTime
        {
            get
            {
                return 10000 * System.DateTime.UtcNow.Minute + 100 * System.DateTime.UtcNow.Second +
                       System.DateTime.UtcNow.Millisecond;
            }
        }

        public static float NextValue
        {
            get { return Singleton._NextValue; }
        }

        public float _NextValue
        {
            get
            {
                if (_seedArray == null || _seedArray.Length == 0)
                    Initialize();

                _seedIndex++;

                if (_seedIndex != _seedArray.Length)
                    return _seedArray[_seedIndex];

                ReinitializeSeed();
                return _seedArray[_seedIndex];
            }
        }

        private void ReinitializeSeed()
        {
            if (_isSeeded)
                _seedIndex = 0;
            else
                Initialize();
        }

        public Dice()
        {
            Initialize();
        }

        public Dice(int seed, int seedCount = 1000)
        {
            Initialize(seed, seedCount);
        }

        public void Initialize()
        {
            _isSeeded = false;
            GenerateSeedArray(PseudoRandomTime);
        }

        public void Initialize(int seed, int seedCount = 1000)
        {
            _isSeeded = true;
            GenerateSeedArray(seed, seedCount);
        }

        private void GenerateSeedArray(int seed, int seedCount = 1000)
        {
            _seed = seed;
            _seedIndex = 0;

            _seedArray = new float[seedCount];
            Random.InitState(seed);

            for (var i = 0; i < seedCount; i++)
                _seedArray[i] = Random.value;

            SetUnitySeedToRandom();
        }

        public static void SetUnitySeedToRandom()
        {
            Random.InitState(PseudoRandomTime);
        }

        public static bool Roll(int chance)
        {
            return Singleton._Roll(chance);
        }

        public bool _Roll(int chance)
        {
            return _NextValue * 100 <= chance ? true : false;
        }

        public static bool Roll(float chance)
        {
            return Singleton._Roll(chance);
        }

        public bool _Roll(float chance)
        {
            return _NextValue <= chance ? true : false;
        }

        public static T RandomMemberOf<T>(IList list, bool removeAfterCheck = false)
        {
            return Singleton._RandomMemberOf<T>(list, removeAfterCheck);
        }

        public T _RandomMemberOf<T>(IList list, bool removeAfterCheck = false)
        {
            if (list.Count == 0)
                return default(T);

            var proxyIndex = _RandomIndexOf(list);
            var returnType = (T) list[proxyIndex];
            if (removeAfterCheck)
                list.RemoveAt(proxyIndex);
            return returnType;
        }

        public static int RandomIndexOf(IList list)
        {
            return Singleton._RandomIndexOf(list);
        }

        public int _RandomIndexOf(IList list)
        {
            if (list.Count == 0) return -1;

            var returnIndex = _Range(0, list.Count);
            return returnIndex;
        }

        public static List<T> Shuffle<T>(IEnumerable<T> original)
        {
            return Singleton._Shuffle(original);
        }

        public List<T> _Shuffle<T>(IEnumerable<T> original)
        {
            var shuffled = new List<T>();
            foreach (var i in original)
                shuffled.Insert(Random.Range(0, shuffled.Count), i);

            return shuffled;
        }

        public static int Range(int startIndex, int endIndex)
        {
            return Singleton._Range(startIndex, endIndex);
        }

        public int _Range(int startIndex, int endIndex)
        {
            var difference = endIndex - startIndex;
            if (difference < 1)
                return startIndex;
            var returnIndex = Mathf.Clamp(startIndex + (int) Mathf.Floor(_NextValue * difference), startIndex,
                endIndex - 1);
            return returnIndex;
        }

        public override string ToString()
        {
            return string.Format("[Dice: isSeeded={0}, seedIndex={1}, seed={2}]", _isSeeded, _seedIndex, _seed);
        }
    }
}