using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace K2
{
    public class Gaussian
    {
        private readonly IRandomProvider _randomProvider;
        private float _savedGaussianValue;
        private bool _hasSavedGaussianValue;

        public Gaussian(IRandomProvider randomProvider)
        {
            _randomProvider = randomProvider;
        }

        private float GetNextGaussianValue()
        {
            if (!_hasSavedGaussianValue)
                return GenerateNextGaussianValue();

            _hasSavedGaussianValue = false;
            return _savedGaussianValue;
        }

        private float GenerateNextGaussianValue()
        {
            // Using a method called Box Muller transform.
            float v1, v2, s;
            do
            {
                v1 = 2 * _randomProvider.Value() - 1;
                v2 = 2 * _randomProvider.Value() - 1;
                s = v1 * v1 + v2 * v2;
            } while (s >= 1 || s == 0);

            var multiplier = Mathf.Sqrt(-2 * Mathf.Log(s) / s);
            _savedGaussianValue = v2 * multiplier;
            _hasSavedGaussianValue = true;
            return v1 * multiplier;
        }

        public float GetGaussianNumber(float mean = 0, float standardDeviation = 1)
        {
            return GetNextGaussianValue() * standardDeviation + mean;
        }

        public int GetGaussianNumber(int mean = 0, int standardDeviation = 1)
        {
            return Mathf.RoundToInt(GetNextGaussianValue() * standardDeviation) + mean;
        }

        public static float GetAverageMean(IEnumerable<float> valueList)
        {
            var sum = valueList.Sum();
            return sum / valueList.Count();
        }

        public static float GetStandardDeviation(IEnumerable<float> valueList)
        {
            var average = GetAverageMean(valueList);
            var sumOfDerivation = valueList.Sum(value => value * value);
            var sumOfDerivationAverage = sumOfDerivation / (valueList.Count() - 1);
            return Mathf.Sqrt(sumOfDerivationAverage - average * average);
        }
    }
}