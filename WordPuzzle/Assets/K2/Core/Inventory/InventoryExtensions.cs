﻿using System.Linq;
using System.Collections.Generic;
using TapSlots.Inventory;

public static class InventoryExtensions
{
    public static void Add<T>(this IInventory<T> inventory, T item)
    {
        Add(inventory, item, 1);
    }

    public static void Add<T>(this IInventory<T> inventory, T item, int count)
    {
        ItemStack<T> itemStack = new ItemStack<T>(item, count);
        inventory.Add(itemStack);
    }

    public static void Remove<T>(this IInventory<T> inventory, T item)
    {
        Remove(inventory, item, 1);
    }

    public static void Remove<T>(this IInventory<T> inventory, T item, int count)
    {
        ItemStack<T> itemStack = new ItemStack<T>(item, count);
        inventory.Remove(itemStack);
    }

    public static bool Contains<T>(this IInventory<T> inventory, T item)
    {
        return Contains(inventory, item, 1);
    }

    public static bool Contains<T>(this IInventory<T> inventory, T item, int minAmount)
    {
        return inventory.ItemSlots.Count(s => s.IsEqualToItem(item)) >= minAmount;
    }

    public static bool Contains<T>(this IInventory<T> inventory, IItemStack<T> item)
    {
        return inventory.ItemSlots.Count(s => s.IsEqualToItem(item)) >= item.Count;
    }

    public static int Count<T>(this IInventory<T> inventory, T item)
    {
        return inventory.ItemSlots.Where(s => s.IsEqualToItem(item)).Sum(s => s.ItemStack.Count);
    }

    public static int Count<T>(this IInventory<T> inventory, IItemStack<T> item)
    {
        return inventory.ItemSlots.Where(s => item.IsEqualToItem(s.ItemStack)).Sum(s => s.ItemStack.Count);
    }

    public static int EmptySlotCount<T>(this IInventoryFixedSize<T> inventory)
    {
        return inventory.ItemSlots.Count(s => s.IsEmpty);
    }

    public static int OccupiedSlotCount<T>(this IInventoryFixedSize<T> inventory)
    {
        return inventory.ItemSlots.Count(s => !s.IsEmpty);
    }

    public static bool IsFull<T>(this IInventoryFixedSize<T> inventory)
    {
        return !inventory.ItemSlots.Any(s => s.IsEmpty);
    }

    public static bool IsEmpty<T>(this IInventoryFixedSize<T> inventory)
    {
        return !inventory.ItemSlots.Any(s => !s.IsEmpty);
    }

    public static IEnumerable<ItemSlot<T>> GetOccupiedSlots<T>(this IInventoryFixedSize<T> inventory)
    {
        return inventory.ItemSlots.Where(s => !s.IsEmpty);
    }
}
