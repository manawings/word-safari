﻿using TapSlots.Inventory;

public static class ItemStackExtensions
{
    public static void Set<T>(this IItemStack<T> self, IItemStack<T> itemSlot)
    {
        self.Count = itemSlot.Count;
    }

    public static void Add<T>(this IItemStack<T> self, IItemStack<T> itemSlot)
    {
        self.Count += itemSlot.Count;
    }

    public static void Remove<T>(this IItemStack<T> self, IItemStack<T> itemSlot)
    {
        self.Count -= itemSlot.Count;
    }

    public static void Set<T>(this IItemStack<T> self, int count)
    {
        self.Count = count;
    }

    public static void Add<T>(this IItemStack<T> self, int count)
    {
        self.Count += count;
    }

    public static void Remove<T>(this IItemStack<T> self, int count)
    {
        self.Count -= count;
    }
}