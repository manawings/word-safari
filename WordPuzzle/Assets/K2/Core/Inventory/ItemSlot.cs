﻿namespace TapSlots.Inventory
{
    public class ItemSlot<T>
    {

        private IItemStack<T> m_itemStack;

        public ItemSlot(IItemStack<T> itemStack = null)
        {
            Set(itemStack);
        }

        public T ItemContent
        {
            get
            {
                return m_itemStack == default(IItemStack<T>) ? default(T) : m_itemStack.Item;
            }
        }

        public IItemStack<T> ItemStack
        {
            get
            {
                return m_itemStack;
            }
        }

        public bool IsEmpty
        {
            get
            {
                return m_itemStack == null;
            }
        }

        public bool CanAdd(IItemStack<T> itemStack)
        {
            return IsEmpty || itemStack.CanStackWith(m_itemStack);
        }

        public bool IsEqualToItem(IItemStack<T> itemStack)
        {
            return m_itemStack != null && m_itemStack.IsEqualToItem(itemStack);
        }

        public bool IsEqualToItem(T item)
        {
            return m_itemStack != null && m_itemStack.IsEqualToItem(item);
        }

        public void Set(IItemStack<T> itemStack)
        {
            m_itemStack = itemStack;
        }

        public void Add(IItemStack<T> itemStack)
        {
            if (IsEmpty)
            {
                m_itemStack = itemStack;
            }
            else
            {
                if (!itemStack.CanStackWith(m_itemStack))
                    throw new System.Exception(string.Format("Cannot add {0} to this slot. Item does not stack.", itemStack));
                m_itemStack.Add(itemStack);
            }
        }

        public void Remove(IItemStack<T> itemStack)
        {
            if (!IsEmpty)
            {
                if (!itemStack.CanStackWith(m_itemStack))
                    throw new System.Exception(string.Format("Cannot remove {0} from this slot. Item does not stack.", itemStack));
                m_itemStack.Remove(itemStack);
                if (m_itemStack.Count == 0)
                    Clear();
            }
        }

        public void Clear()
        {
            m_itemStack = null;
        }

        public override string ToString()
        {
            return string.Format("[{0}] {1}", "Slot", IsEmpty ? "Empty" : m_itemStack.ToString());
        }
    }
}
