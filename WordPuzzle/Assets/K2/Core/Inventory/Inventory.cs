﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TapSlots.Inventory
{
    public interface IInventory<T>
    {
        IList<ItemSlot<T>> ItemSlots { get; }
        void Add(IItemStack<T> itemStack);
        void Remove(IItemStack<T> itemStack);
        void Clear();
    }

    public interface IInventoryFixedSize<T> : IInventory<T>
    {
        int MaxSize { get; }
    }

    public class FixedInventory<T> : IInventoryFixedSize<T>
    {
        protected ItemSlot<T>[] m_itemSlots;
        protected int m_size;
        public IList<ItemSlot<T>> ItemSlots
        {
            get
            {
                return m_itemSlots;
            }
        }

        public int MaxSize
        {
            get
            {
                return m_size;
            }
        }

        public FixedInventory(int size)
        {
            m_size = size;
            m_itemSlots = new ItemSlot<T>[m_size];
            for (int i = 0; i < m_size; i++)
                m_itemSlots[i] = new ItemSlot<T>();
        }

        public void Add(IItemStack<T> item)
        {
            ItemSlot<T> slot = ItemSlots.FirstOrDefault(s => s.IsEmpty || s.CanAdd(item));
            if (slot == null)
                throw new Exception("Item cannot be added to this inventory. Inventory has no empty slots left.");

            slot.Add(item);
        }

        public void Remove(IItemStack<T> item)
        {
            ItemSlot<T> slot = ItemSlots.FirstOrDefault(s => !s.IsEmpty && item.CanStackWith(s.ItemStack));
            if (slot != null)
                slot.Remove(item);
        }

        public void Clear()
        {
            foreach (ItemSlot<T> slot in ItemSlots)
                slot.Clear();
        }
    }

    public class FlexInventory<T> : IInventory<T>
    {
        protected List<ItemSlot<T>> m_itemSlots;
        public IList<ItemSlot<T>> ItemSlots
        {
            get
            {
                return m_itemSlots;
            }
        }

        public FlexInventory()
        {
            m_itemSlots = new List<ItemSlot<T>>();
        }

        public void Add(IItemStack<T> item)
        {
            ItemSlot<T> slot = ItemSlots.FirstOrDefault(s => s.IsEmpty || s.CanAdd(item));
            if (slot != null)
                slot.Add(item);
            else
                m_itemSlots.Add(new ItemSlot<T>(item));
        }

        public void Remove(IItemStack<T> item)
        {
            ItemSlot<T> slot = ItemSlots.FirstOrDefault(s => !s.IsEmpty && item.CanStackWith(s.ItemStack));
            if (slot != null)
            {
                slot.Remove(item);
                if (slot.IsEmpty)
                    m_itemSlots.Remove(slot);
            }
        }

        public void Clear()
        {
            m_itemSlots.Clear();
        }
    }
}
