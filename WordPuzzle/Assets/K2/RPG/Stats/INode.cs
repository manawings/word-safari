﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace K2.RPG.Stats
{
    public interface INode<T>
    {
        INode<T> InputNode { get; set; }
        INode<T> OutputNode { get; set; }
        T OutputValue { get; }
        INodeProcessor<T> Processor { get; set; }
    }

    public interface INodeProcessor<T>
    {
        T Process(T inputValue);
    }

    public static class NodeExtentions
    {
        public static void ConnectTo<T>(this INode<T> fromNode, INode<T> toNode)
        {
            fromNode.OutputNode = toNode;
            toNode.InputNode = fromNode;
        }

        public static void Process<T>(this INode<T> node)
        {
            while (true)
            {
                if (node.InputNode != null)
                    node.Processor.Process(node.InputNode.OutputValue);

                if (node.OutputNode != null)
                {
                    node = node.OutputNode;
                    continue;
                }
                break;
            }
        }
    }
}
