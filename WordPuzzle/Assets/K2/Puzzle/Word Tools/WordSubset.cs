﻿using System.Collections.Generic;
using System.Linq;

public class WordSubset {

    public bool Subset(string _small, string _big)
    {
        var smallList = _small.ToList();
        var bigList = _big.ToList();

        for (var i = smallList.Count - 1; i >= 0; i--)
        {
            var c = smallList[i];
            if (!bigList.Contains(c))
                continue;

            bigList.Remove(c);
            smallList.RemoveAt(i);
        }

        return smallList.Count == 0;
    }
}
