﻿using NUnit.Framework;
namespace UnityTest
{
    [TestFixture]
    internal class UT_WordIntersector
    {
        [Test]
        public void PassingTest()
        {
            Assert.Pass();
        }

        [Test]
        public void FindsIntersection()
        {
            WordIntersector intersector = new WordIntersector("APPLE", "LAP");
            var count = intersector.Count();
            Assert.AreEqual(4, count);
        }

        [Test]
        public void Subset()
        {
            var subset = new WordSubset();
            Assert.AreEqual(true, subset.Subset("LAP", "APPLE"));
            Assert.AreEqual(true, subset.Subset("EBE", "BEE"));
            Assert.AreEqual(true, subset.Subset("EASTER", "YESTERDAY"));
            Assert.AreEqual(true, subset.Subset("E", "EE"));
            Assert.AreEqual(false, subset.Subset("ABC", "ABE"));
            Assert.AreEqual(false, subset.Subset("EE", "E"));
            Assert.AreEqual(false, subset.Subset("APPLE", "LAP"));
        }
    }
}