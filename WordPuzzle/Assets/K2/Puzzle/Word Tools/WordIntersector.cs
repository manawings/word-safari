﻿public class WordIntersector
{
    private string _a, _b;
    private int _count = 0;

    public WordIntersector(string a, string b)
    {
        _a = a;
        _b = b;
    }

    private void CalculateIntersections()
    {
        _count = 0;
        for (var i = 0; i < _a.Length; i++)
        {
            for (var j = 0; j < _b.Length; j++)
            {
                if (_a[i] == _b[j])
                    _count++;
            }
        }
    }

    public int Count()
    {
        CalculateIntersections();
        return _count;
    }

}
