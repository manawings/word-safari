﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WordGameGenerator : MonoBehaviour
{

    [SerializeField] private TextAsset _textAsset;

    private IList<string> _wordList;

	// Use this for initialization
	void Start ()
	{
	    ExtractText();
	    GetMatchesForWord("melon", 3);
	  //  GetMatchesForWord("apple", 3);
	    //GetMatchesForWord("fight", 3);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void ExtractText()
    {
        _wordList = CSVParser.ReadAsList1D(_textAsset.text);
        Debug.Log(string.Format("Extracted Word List: {0}", _wordList.Count));
    }

    private void GetMatchesForWord(string source, int minLength)
    {
        Debug.Log(string.Format("Getting Matches for {0}: ---------", source));
        var matchList = new List<string>();
        var wordSubset = new WordSubset();
        for (var i = 0; i < _wordList.Count; i++)
        {
            if (_wordList[i].Length < minLength)
                continue;

            if (!wordSubset.Subset(_wordList[i], source))
                continue;

            matchList.Add(_wordList[i]);
            Debug.Log(string.Format("Found {0}: {1}", i, _wordList[i]));
        }
    }
}
