using System.Collections.Generic;
using System.Linq;

namespace K2.Word
{
    public class LetterPuzzle
    {
        private readonly SingleLetter[] _letters;
        public string CurrentWord = string.Empty;
        public string Master { get; private set; }
        public LetterPuzzle(string word)
        {
            Master = word;
            _letters = new SingleLetter[word.Length];
            for (int i = 0, m = word.Length; i < m; i++)
            {
                var letter = new SingleLetter(word[i].ToString());
                _letters[i] = letter;
            }
        }

        private SingleLetter FirstValidLetter(string letter)
        {
            return _letters.FirstOrDefault(p => p.Letter == letter && !p.IsComplete);
        }

        public bool CanPlayLetter(string letter)
        {
            return FirstValidLetter(letter) != null;
        }

        public void PlayLetter(string letter)
        {
            var validLetter = FirstValidLetter(letter);
            if (validLetter == null)
                return;

            CurrentWord += letter;
            validLetter.IsComplete = true;
        }

        public void Reset()
        {
            foreach (var singleLetter in _letters)
                singleLetter.IsComplete = false;

            CurrentWord = string.Empty;;
        }

        public string Release()
        {
            var word = CurrentWord;
            Reset();
            return word;
        }

        internal class SingleLetter
        {
            public SingleLetter(string letter)
            {
                Letter = letter;
            }

            public string Letter;
            public bool IsComplete;
        }
    }
}