﻿using K2.Word;
using NUnit.Framework;

namespace UnityTest
{

    [TestFixture]
    internal class UT_LetterPuzzle
    {
        private LetterPuzzle _letterPuzzle;

        [SetUp]
        public void Setup()
        {
            _letterPuzzle = new LetterPuzzle("bloom");
        }

        [Test]
        public void CanCheckValidLetters()
        {
            Assert.IsTrue(_letterPuzzle.CanPlayLetter("b"));
            Assert.IsTrue(_letterPuzzle.CanPlayLetter("l"));
            Assert.IsTrue(_letterPuzzle.CanPlayLetter("o"));
            Assert.IsTrue(_letterPuzzle.CanPlayLetter("m"));
        }

        [Test]
        public void CanPlayLetters()
        {
            _letterPuzzle.PlayLetter("l");
            _letterPuzzle.PlayLetter("o");
            _letterPuzzle.PlayLetter("o");
            _letterPuzzle.PlayLetter("m");
            Assert.AreEqual("loom", _letterPuzzle.Release());
            Assert.AreEqual("", _letterPuzzle.Release());
        }

        [Test]
        public void CannotPlayInvalidLetters()
        {
            Assert.IsFalse(_letterPuzzle.CanPlayLetter("a"));
            Assert.IsFalse(_letterPuzzle.CanPlayLetter("e"));
            Assert.IsFalse(_letterPuzzle.CanPlayLetter("x"));
            Assert.IsFalse(_letterPuzzle.CanPlayLetter("q"));
        }

        [Test]
        public void CannotOverplayALetter()
        {
            Assert.IsTrue(_letterPuzzle.CanPlayLetter("o"));
            _letterPuzzle.PlayLetter("o");
            _letterPuzzle.PlayLetter("o");
            Assert.IsFalse(_letterPuzzle.CanPlayLetter("o"));
        }
    }

    [TestFixture]
    internal class UT_WordCookie
    {
        private WordChecklist _checklist;

        [SetUp]
        public void Setup()
        {
            _checklist = new WordChecklist("bad", "bat", "frog", "apple");
        }

        [Test]
        public void PuzzleInputIsValid()
        {
            Assert.IsTrue(_checklist.IsWordValid("frog"));
            Assert.IsTrue(_checklist.CanWordBePlayed("frog"));
        }

        [Test]
        public void PuzzleInputIsNotValid()
        {
            Assert.IsFalse(_checklist.IsWordValid("fog"));
            Assert.IsFalse(_checklist.CanWordBePlayed("fog"));
        }

        [Test]
        public void InputCanComplete()
        {
            var word = "frog";
            Assert.IsTrue(_checklist.IsWordValid(word));
            Assert.IsFalse(_checklist.IsWordComplete(word));
            _checklist.CompleteWord(word);
            Assert.IsTrue(_checklist.IsWordComplete(word));
        }

        [Test]
        public void ProgressIsAccurate()
        {
            _checklist.CompleteWord("bad");
            _checklist.CompleteWord("bat");
            _checklist.CompleteWord("frog");

            Assert.AreEqual(0.75f, _checklist.Progress);
            Assert.AreEqual(4, _checklist.TotalTargets);
            Assert.AreEqual(3, _checklist.CompletedTargets);
        }

        [Test]
        public void CanBeCompleted()
        {
            Assert.IsFalse(_checklist.IsComplete);

            _checklist.CompleteWord("bad");
            _checklist.CompleteWord("bat");
            _checklist.CompleteWord("frog");
            _checklist.CompleteWord("apple");

            Assert.IsTrue(_checklist.IsComplete);
        }

        [Test]
        public void CompletedWordCannotBePlayedTwice()
        {
            Assert.IsTrue(_checklist.CanWordBePlayed("frog"));
            _checklist.CompleteWord("frog");
            Assert.IsFalse(_checklist.CanWordBePlayed("frog"));
        }
    }
}
