﻿using System.Collections.Generic;
using System.Linq;

namespace K2.Word
{
    public class WordChecklist
    {
        private readonly IList<WordTarget> _wordTargets;

        public WordChecklist(params string[] words)
        {
            _wordTargets = new List<WordTarget>();
            for (int i = 0, m = words.Length; i < m; i++)
                AddWordTarget(words[i]);
        }

        private void AddWordTarget(string word)
        {
            var wordTarget = new WordTarget(word);
            WordTargets.Add(wordTarget);
        }

        public WordTarget GetFirstValidTarget(string word)
        {
            for (int i = 0, m = WordTargets.Count; i < m; i++)
            {
                if (WordTargets[i].IsWordValid(word))
                    return WordTargets[i];
            }

            return null;
        }

        public bool IsWordValid(string word)
        {
            return GetFirstValidTarget(word) != null;
        }

        public int IndexOfWordValid(string word)
        {
            for (int i = 0, m = WordTargets.Count; i < m; i++)
            {
                if (WordTargets[i].IsWordValid(word))
                {
                    WordTargets[i].Complete();
                    return i;
                }
            }
            return -1;
        }

        public bool IsWordComplete(string word)
        {
            var target = GetFirstValidTarget(word);
            return target != null && target.IsComplete;
        }

        public bool CanWordBePlayed(string word)
        {
            return IsWordValid(word) && !IsWordComplete(word);
        }

        public void CompleteWord(string word)
        {
            var target = GetFirstValidTarget(word);
            target.Complete();
        }

        public int TotalTargets
        {
            get { return WordTargets.Count; }
        }

        public int CompletedTargets
        {
            get { return WordTargets.Count(p => p.IsComplete); }
        }

        public float Progress
        {
            get { return (float) CompletedTargets / TotalTargets; }
        }

        public bool IsComplete
        {
            get { return Progress == 1; }
        }

        public IList<WordTarget> WordTargets
        {
            get
            {
                return _wordTargets;
            }
        }
    }
}