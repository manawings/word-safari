﻿namespace K2.Word
{
    public class WordTarget
    {
        public string Word;
        public bool IsComplete;

        public bool IsWordValid(string word)
        {
            return Word.Equals(word);
        }

        public WordTarget(string word)
        {
            Word = word;
            IsComplete = false;
        }

        public void Complete()
        {
            IsComplete = true;
        }
    }
}