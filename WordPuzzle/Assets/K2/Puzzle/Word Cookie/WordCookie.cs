﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace K2.Word
{
    public class WordCookie
    {
        private LetterPuzzle _letterPuzzle;
        private WordChecklist _wordChecklist;

        public WordCookie()
        {
            _letterPuzzle = new LetterPuzzle("melting");
            _wordChecklist = new WordChecklist("met", "get", "let", "melt", "lint", "ten", "gem");
        }

        public void InputLetter(string letter)
        {
            if (_letterPuzzle.CanPlayLetter(letter))
                _letterPuzzle.PlayLetter(letter);
        }

        public string GetCurrentWord()
        {
            return _letterPuzzle.CurrentWord;
        }

        public string GetMaster()
        {
            return _letterPuzzle.Master;
        }

        public void Release()
        {
            var word = _letterPuzzle.Release();
            if (_wordChecklist.CanWordBePlayed(word))
            {
                _wordChecklist.CompleteWord(word);
            }
            else
            {
                
            }
        }

        public bool HasWon()
        {
            return _wordChecklist.IsComplete;
        }

        public float Progress
        {
            get { return _wordChecklist.Progress; }
        }
    }
}
