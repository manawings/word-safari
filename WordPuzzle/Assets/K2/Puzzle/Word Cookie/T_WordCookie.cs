﻿using UnityEngine;
using System.Collections;
using K2.Word;
using UnityEngine.UI;

public class T_WordCookie : MonoBehaviour
{


    [SerializeField] private Text _master, _entry, _progress;

    private WordCookie _wordCookie;

	void Start () {
        _wordCookie = new WordCookie();
	    RefreshUi();
	}
	
	void Update () {
	    if (Input.anyKeyDown)
	    {
	        if (Input.GetKeyDown(KeyCode.Space))
	        {
                _wordCookie.Release();
            }
	        else
	        {
	            if (Input.inputString.Length > 0)
	            {
	                string key = Input.inputString[0].ToString().ToLower();
	                _wordCookie.InputLetter(key);
	            }
	        }
	        RefreshUi();
	    }
	}

    void RefreshUi()
    {
        _master.text = _wordCookie.GetMaster();
        _entry.text = _wordCookie.GetCurrentWord();
        _progress.text = Mathf.RoundToInt(_wordCookie.Progress * 100).ToString() + "%";
    }
}
